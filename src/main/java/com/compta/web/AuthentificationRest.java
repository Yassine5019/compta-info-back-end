package com.compta.web;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.compta.entities.User;
import com.compta.jwt.JwtUtils;
import com.compta.entities.ERole;
import com.compta.entities.Role;
import com.compta.dto.request.LoginRequest;
import com.compta.dto.response.JwtResponse;
import com.compta.service.UserDetailsImpl;
import com.compta.dao.*;
import com.compta.dto.request.Register;
import com.compta.dto.response.ResponseMessage;

@CrossOrigin(origins = "*", maxAge = 36000)
@RestController
@RequestMapping("/api/auth")
public class AuthentificationRest {
	@Autowired
	private userRepository serviceUser;
	
	@Autowired
	private PasswordEncoder encode;
	
	@Autowired
	private roleRepository serviceRole;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private JwtUtils jwtUtils;
	
	@PostMapping("register")
	public ResponseEntity<ResponseMessage> registerUser(@Valid @RequestBody Register registerRequest){
		if(serviceUser.existsByUsername(registerRequest.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Username is already taken !"), HttpStatus.BAD_REQUEST);
		}
		if(serviceUser.existsByEmail(registerRequest.getEmail())) {
			return new ResponseEntity<>(new ResponseMessage("Email is already taken !"), HttpStatus.BAD_REQUEST);
		}
		
		User user=new User(registerRequest.getName(), encode.encode(registerRequest.getPassword()), registerRequest.getEmail(), registerRequest.getUsername());
		Set<String> rolesInRequest=registerRequest.getRoles();
		Set<Role> roles=new HashSet<>();
		
		if (rolesInRequest == null) {
			Role role1 = serviceRole.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(role1);
		} else {
		rolesInRequest.forEach(role->{
			switch(role) {
			case "admin":
				Role adminRole = serviceRole.findByName(ERole.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(adminRole);

				break;
			case "mod":
				Role modRole = serviceRole.findByName(ERole.ROLE_MODERATOR)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(modRole);

				break;
			default:
				Role userRole = serviceRole.findByName(ERole.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(userRole);
		   }
		});
	 }
		user.setListeRoles(roles);
		serviceUser.save(user);
	  return new ResponseEntity<>(new ResponseMessage("Registration User"), HttpStatus.OK);
  }
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
//		List<String> roles = userDetails.getAuthorities().stream()
//				.map(item -> item.getAuthority())
//				.collect(Collectors.toList());
		
		return ResponseEntity.ok(new JwtResponse(jwt, 
				 userDetails.getId(), 
				 userDetails.getUsername(), 
				 userDetails.getEmail()));

//		return ResponseEntity.ok(new JwtResponse(jwt, 
//												 userDetails.getId(), 
//												 userDetails.getUsername(), 
//												 userDetails.getEmail(), 
//												 roles));
	}
}
