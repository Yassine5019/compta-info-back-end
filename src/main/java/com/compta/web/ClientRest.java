package com.compta.web;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.compta.dao.clientRepository;
import com.compta.dao.userRepository;
import com.compta.entities.*;
import com.compta.exception.ResourceNotFoundException;

@RestController
@CrossOrigin(origins = "*", maxAge = 36000)
@RequestMapping("/api/clients")
public class ClientRest {

	@Autowired
	clientRepository clientRepository;
	
	@Autowired
	userRepository serviceUser;
		
	@RequestMapping(path="/listeClients", method=RequestMethod.GET)
	//@PreAuthorize("hasRole('ROLE_USER')")
	public List<Client> listClient(){
		return clientRepository.findAll();	
	}
	
	@RequestMapping(path = "/client/{id}", method = RequestMethod.GET)
	//@PreAuthorize("hasRole(USER)")
	public ResponseEntity<Client> getClient(@PathVariable Long id) throws ResourceNotFoundException{
			Client client = clientRepository.findById(id)
			          .orElseThrow(() -> new ResourceNotFoundException("Client not found for this id :: " + id));
			        return ResponseEntity.ok().body(client);
		
	}
	
	@RequestMapping(value="/saveClient", method=RequestMethod.POST)
	//@PreAuthorize("hasRole(USER) or hasRole(ADMIN)")
	public Client saveClient(@RequestBody Client c) {
		Client cl=new Client();
		org.springframework.security.core.Authentication authentication=SecurityContextHolder.getContext().getAuthentication();
		Optional<User> user=serviceUser.findByUsername(authentication.getName());
		Client client=new Client(c.getCodeClient(), c.getDateCloture(), c.getFormeJuridique(), user.isPresent() ? user.get() : null);
		try {
			cl= clientRepository.save(client);
		} catch (Exception e) {
			e.getMessage();
		}
		return cl;	
	}
	
	//@RequestBody : chercher via la requete http les informations sous format json et on va les stocker ds l'objet c
	@RequestMapping(path = "/updateClient/{id}", method = RequestMethod.PUT)
	//@PreAuthorize("hasRole(USER) or hasRole(ADMIN)")
	public Client updateClient(@RequestBody Client c, @PathVariable Long id) {
		Client cl = new Client();
		if (id == null)
			return null;
		c.setId(id);
		try {
			cl = clientRepository.save(c);
		} catch (Exception e) {
			e.getMessage();
		}
		return cl;
	}
	
	@RequestMapping(path="/deleteClient/{id}", method=RequestMethod.DELETE)
	//@PreAuthorize("hasRole(ADMIN)")
	public boolean suuprimerClient(@PathVariable Long id) {
		clientRepository.deleteById(id);
		return true;
	}
		
	}

