package com.compta.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.compta.entities.Client;
public interface clientRepository extends JpaRepository<Client, Long> {
	@Query("select c from Client c where c.codeClient like :x")
	Page<Client> chercher(@org.springframework.data.repository.query.Param("x")String mc, org.springframework.data.domain.Pageable pageable);
}
