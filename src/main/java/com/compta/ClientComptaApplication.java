package com.compta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientComptaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientComptaApplication.class, args);
	}

}
