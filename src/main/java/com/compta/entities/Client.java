package com.compta.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
public class Client implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	private int codeClient;
	
	@javax.persistence.Temporal(TemporalType.DATE)
	private Date dateCloture;
	
	private String formeJuridique;
	
	@ManyToOne
	private User user;
	
	@ManyToOne
	private Collaborateur collaborateur;
	public Client(int client, Date date, String formeJuridique, User user) {
		this.codeClient=client;
		this.dateCloture=date;
		this.formeJuridique=formeJuridique;
		this.user=user;
	}
	public int getCodeClient() {
		return codeClient;
	}
	public void setCodeClient(int codeClient) {
		this.codeClient = codeClient;
	}
	public Date getDateCloture() {
		return dateCloture;
	}
	public void setDateCloture(Date dateCloture) {
		this.dateCloture = dateCloture;
	}
	public String getFormeJuridique() {
		return formeJuridique;
	}
	public void setFormeJuridique(String formeJuridique) {
		this.formeJuridique = formeJuridique;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Collaborateur getCollaborateur() {
		return collaborateur;
	}
	public void setCollaborateur(Collaborateur collaborateur) {
		this.collaborateur = collaborateur;
	}
	public Client(int codeClient, Date dateCloture, String formeJuridique, User user, Collaborateur collaborateur) {
		super();
		this.codeClient = codeClient;
		this.dateCloture = dateCloture;
		this.formeJuridique = formeJuridique;
		this.user = user;
		this.collaborateur = collaborateur;
	}
	@Override
	public String toString() {
		return "Client [codeClient=" + codeClient + ", dateCloture=" + dateCloture + ", formeJuridique="
				+ formeJuridique + ", user=" + user + ", collaborateur=" + collaborateur + "]";
	}
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    
	

}
