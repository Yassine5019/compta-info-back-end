package com.compta.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compta.entities.ERole;
import com.compta.entities.Role;
import com.compta.entities.User;

public interface userRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
	boolean existsByUsername(String username);
	boolean existsByEmail(String email);
	//Optional<Role> findByName(String roleAdmin);

}
