package com.compta.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compta.entities.ERole;
import com.compta.entities.Role;

public interface roleRepository extends JpaRepository<Role, Long> {

	Optional<Role> findByName(ERole roleUser);

}
