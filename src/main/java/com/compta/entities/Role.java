package com.compta.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
//@Data
@ToString
//@AllArgsConstructor
//@NoArgsConstructor
@Table(name = "roles")
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToMany(mappedBy="listeRoles")
	private Set<User> listeUser=new HashSet<User>();
	
	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private ERole name;

	public Set<User> getListeUser() {
		return listeUser;
	}

	public void setListeUser(Set<User> listeUser) {
		this.listeUser = listeUser;
	}

	public ERole getName() {
		return name;
	}

	public void setName(ERole name) {
		this.name = name;
	}

	public Role(Set<User> listeUser, ERole name) {
		super();
		this.listeUser = listeUser;
		this.name = name;
	}

	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
